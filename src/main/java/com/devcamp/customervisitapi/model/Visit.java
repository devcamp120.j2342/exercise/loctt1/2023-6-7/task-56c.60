package com.devcamp.customervisitapi.model;

import java.util.Date;

public class Visit {
    private Date date;
    private double serviceExpense;
    private double productExpense;
    private Customer customer;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getServiceExpense() {
        return serviceExpense;
    }

    public void setServiceExpense(double serviceExpense) {
        this.serviceExpense = serviceExpense;
    }

    public double getProductExpense() {
        return productExpense;
    }

    public void setProductExpense(double productExpense) {
        this.productExpense = productExpense;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Visit(Date date, Customer customer) {
        this.date = date;
        this.customer = customer;
    }

    public Visit(Date date, double serviceExpense, double productExpense) {
        this.date = date;
        this.serviceExpense = serviceExpense;
        this.productExpense = productExpense;
    }

    public Visit(Date date, double serviceExpense, double productExpense, Customer customer) {
        this.date = date;
        this.serviceExpense = serviceExpense;
        this.productExpense = productExpense;
        this.customer = customer;
    }

    public Visit() {
    }

    @Override
    public String toString() {
        return "Visit [date=" + date + ", serviceExpense=" + serviceExpense + ", productExpense=" + productExpense
                + ", customer=" + customer + "]";
    }

    public double getTotalExpense() {
        return this.productExpense + this.serviceExpense;
    }

}
