package com.devcamp.customervisitapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customervisitapi.model.Visit;
import com.devcamp.customervisitapi.service.VisitService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class VisitController {
    @Autowired
    private VisitService visitService;

    @GetMapping("/visits")
    public ArrayList<Visit> visitsList() {
        ArrayList<Visit> visits = visitService.allVisits();
        return visits;
    }

}
