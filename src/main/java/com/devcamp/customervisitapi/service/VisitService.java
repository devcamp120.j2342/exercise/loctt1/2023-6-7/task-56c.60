package com.devcamp.customervisitapi.service;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.customervisitapi.model.Visit;

@Service
public class VisitService {
    @Autowired
    private CustomerService customerService;
    Visit visit1 = new Visit(new Date(), 100.5, 87.5);
    Visit visit2 = new Visit(new Date(), 18.5, 87.5);
    Visit visit3 = new Visit(new Date(), 10.5, 87.5);

    public ArrayList<Visit> allVisits() {
        visit1.setCustomer(customerService.customer1);
        visit2.setCustomer(customerService.customer2);
        visit3.setCustomer(customerService.customer3);
        ArrayList<Visit> visits = new ArrayList<>();
        visits.add(visit1);
        visits.add(visit2);

        visits.add(visit3);
        return visits;

    }

}
